import moment from "moment";
import objectHash from "object-hash";

export class Entity {
    /* Unique identifier */
    public id: string = "";

    /* Data source (dropbox, calendar, etc) */
    public source: string = "";

    /* Used for simple search matching */
    public matching_terms: string[] = [];

    /* Used for sorting by time */
    public __ts: number;

    /* Flexible schema */
    [prop: string]: any;

    constructor(data: Partial<Entity> = {}) {
        const defaultData = {
            id: objectHash(data),
        };

        Object.assign(this, { ...defaultData, ...data });

        switch (data.source) {
            case "calendar":
                this.__ts = moment(data.date || Date.now()).valueOf();
                break;
            case "contacts":
                this.__ts = moment(data.last_contact || Date.now()).valueOf();
                break;
            case "dropbox":
                this.__ts = moment(data.created || Date.now()).valueOf();
                break;
            case "slack":
                this.__ts = moment(data.timestamp || Date.now()).valueOf();
                break;
            case "twitter":
                this.__ts = moment(data.timestamp || Date.now()).valueOf();
                break;
            default:
                this.__ts = Date.now();
        }
    }
}
