import { Entity } from "src/entity/entity";

export class EntityStore {
    public static instance() {
        if (!EntityStore._instance) {
            EntityStore._instance = new EntityStore();
        }

        return EntityStore._instance;
    }

    private static _instance: EntityStore;

    public indexById: Map<string, Entity> = new Map();
    public indexByTerm: Map<string, Set<string>> = new Map();

    public insert(entity: Entity) {
        const inserted = !this.indexById.has(entity.id);
        this.indexById.set(entity.id, entity);

        for (const term of entity.matching_terms) {
            if (!this.indexByTerm.has(term)) {
                this.indexByTerm.set(term, new Set());
            }

            (this.indexByTerm.get(term) as Set<string>).add(entity.id);
        }
        return inserted;
    }

    public clear() {
        this.indexById.clear();
        this.indexByTerm.clear();
    }

    public get size() {
        return this.indexById.size;
    }

    public lookup(id: string) {
        return this.indexById.get(id);
    }

    public search(terms: string[]): Entity[] {
        let results = new Set<string>();

        /** Select results that appear in all term indices (Boolean AND behavior) */
        for (const term of terms) {
            const termMatches = this.indexByTerm.get(term);
            if (termMatches) {
                if (!results.size) {
                    // first term checked, so all matches should go into result set
                    results = termMatches;
                } else {
                    // do intersection on current result set and set of entities that match current term
                    results = new Set([...results].filter(id => termMatches.has(id)));
                }
            } else {
                // no hits for this term, so boolean AND expression fails; meaning
                // there are no results
                results.clear();
                break;
            }
        }

        // Map id values from result set to actual Entity objects
        const entities = [...results].filter(id => this.indexById.has(id)).map(id => this.indexById.get(id) as Entity);

        // Sort by __ts field, newest to oldest
        return entities.sort((a, b) => {
            return b.__ts - a.__ts;
        });
    }
}
