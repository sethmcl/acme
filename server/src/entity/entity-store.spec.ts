import { Entity } from "./entity";
import { EntityStore } from "./entity-store";

describe("EntityStore", () => {
    let store: EntityStore;

    beforeEach(() => {
        store = new EntityStore();
    });

    it("Can insert and retrieve entity", () => {
        const entity = new Entity({
            matching_terms: ["abra", "cadabra"],
            source: "test-suite",
        });

        store.insert(entity);
        expect(entity).toBe(store.lookup(entity.id));
    });

    it("Can clear store", () => {
        const entity = new Entity({
            matching_terms: ["abra", "cadabra"],
            source: "test-suite",
        });

        store.insert(entity);
        expect(store.size).toBe(1);
        store.clear();
        expect(store.size).toBe(0);
    });

    describe("Search", () => {
        it("Can search for entity", () => {
            const entity = new Entity({
                matching_terms: ["abra", "cadabra"],
                source: "test-suite",
            });

            store.insert(entity);
            const result = store.search(["abra"]);
            expect(result.length).toBe(1);
            expect(result[0]).toBe(entity);
        });

        it("Boolean search (expect no result)", () => {
            const entity = new Entity({
                matching_terms: ["abra", "cadabra"],
                source: "test-suite",
            });

            store.insert(entity);
            const result = store.search(["abra", "dibiz"]);
            expect(result.length).toBe(0);
        });

        it("Boolean search (expect single result)", () => {
            const expectedMatch = new Entity({
                matching_terms: ["john", "cadavera", "homeland"],
                source: "test-suite",
            });

            store.insert(expectedMatch);
            store.insert(
                new Entity({
                    matching_terms: ["john", "abc"],
                    source: "test-suite",
                }),
            );
            store.insert(
                new Entity({
                    matching_terms: ["cadavera", "emf"],
                    source: "test-suite",
                }),
            );

            const result = store.search(["cadavera", "john"]);
            expect(result.length).toBe(1);
            expect(result[0]).toBe(expectedMatch);
        });
    });
});
