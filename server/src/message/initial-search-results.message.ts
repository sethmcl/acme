import { Entity } from "src/entity/entity";
import { Message, MessageType } from "./types";

interface MessageData {
    liveSearchId: string;
    results: Entity[];
}

export class InitialSearchResultsMessage implements Message {
    public type = MessageType.InitialSearchResults;
    public data: MessageData;

    constructor(data: MessageData) {
        this.data = data;
    }
}
