import * as log from "src/log";
import WebSocket from "ws";

export const sendFactory = (socket: WebSocket) => (data: any) => sendMessage(socket, data);
export const sendMessage = (socket: WebSocket, data: any) => {
    try {
        const json = JSON.stringify(data);
        socket.send(json);
    } catch (e) {
        log.error(`Unable to send message data: ${data}`);
        throw e;
    }
};
