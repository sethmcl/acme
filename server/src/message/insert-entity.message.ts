import { Entity } from "src/entity/entity";
import { Message, MessageType } from "./types";

type MessageData = Entity;

export class InsertEntityMessage implements Message {
    public type = MessageType.InitialSearchResults;
    public data: MessageData;

    constructor(data: MessageData) {
        this.data = data;
    }
}
