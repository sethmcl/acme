import { Message, MessageType } from "./types";

interface MessageData {
    liveSearchId: string;
}

export class ResumeLiveSearchMessage implements Message {
    public type = MessageType.ResumeLiveSearch;
    public data: MessageData;

    constructor(data: MessageData) {
        this.data = data;
    }
}
