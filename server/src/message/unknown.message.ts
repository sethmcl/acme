import { Message, MessageType } from "./types";

type MessageData = any;

export class UnknownMessage implements Message {
    public type = MessageType.Unknown;
    public data: MessageData;

    constructor(data: MessageData) {
        this.data = data;
    }
}
