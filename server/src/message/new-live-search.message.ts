import { Message, MessageType } from "./types";

interface MessageData {
    query: string;
}

export class NewLiveSearchMessage implements Message {
    public type = MessageType.NewLiveSearch;
    public data: MessageData;

    constructor(data: MessageData) {
        this.data = data;
    }
}
