import { Message, MessageType } from "./types";

type MessageData = void;

export class ClearEntitiesMessage implements Message {
    public type = MessageType.InitialSearchResults;
    public data: MessageData;
}
