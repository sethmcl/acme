import * as WebSocket from "ws";

export type MessageHandler = (socket: WebSocket, message: Message) => void;

export enum MessageType {
    /* Received by server from a data source. Contains new entity to add to store */
    InsertEntity = "insert-entity",

    /* Delete all stored entities */
    ClearEntities = "clear-entities",

    /* Request from client to start a new live search */
    NewLiveSearch = "new-live-search",

    /* Pause an existing live search */
    PauseLiveSearch = "pause-live-search",

    /* Resume an existing live search */
    ResumeLiveSearch = "resume-live-search",

    /* Emit new live search result to an existing subscribed client */
    NewLiveSearchResult = "new-live-search-result",

    /* Initial search results payload send back to client when a new search is requested */
    InitialSearchResults = "initial-search-results",

    /* Unknown type */
    Unknown = "unknown",
}

export interface SerializedMessage {
    type: MessageType;
    data: Record<string, any>;
}

export interface Message {
    type: MessageType;
    data: any;
}
