import { Entity } from "src/entity/entity";
import { Message, MessageType } from "./types";

interface MessageData {
    liveSearchId: string;
    result: Entity;
}

export class NewLiveSearchResultMessage implements Message {
    public type = MessageType.NewLiveSearchResult;
    public data: MessageData;

    constructor(data: MessageData) {
        this.data = data;
    }
}
