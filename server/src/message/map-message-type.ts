import { MessageType } from "./types";

export const mapMessageType = (data: any) => {
    let mType = MessageType.Unknown;
    for (const key of Object.keys(MessageType)) {
        if (MessageType[key] === data.type) {
            mType = MessageType[key];
            break;
        }
    }
    return mType;
};
