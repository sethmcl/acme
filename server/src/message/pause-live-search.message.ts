import { Message, MessageType } from "./types";

interface MessageData {
    liveSearchId: string;
}

export class PauseLiveSearchMessage implements Message {
    public type = MessageType.PauseLiveSearch;
    public data: MessageData;

    constructor(data: MessageData) {
        this.data = data;
    }
}
