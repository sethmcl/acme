import * as log from "./log";
import { init } from "./server";

(async function bootstrap() {
    const port = await init();
    log.info(`Server is listening on port ${port}`);
})();
