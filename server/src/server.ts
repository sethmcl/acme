import * as http from "http";
import * as log from "src/log";
import WebSocket from "ws";
import { handlers } from "./handler";
import { LiveSearchStore } from "./live-search/live-search-store";
import { mapMessageType } from "./message";

export const init = async () => {
    /* http server reference */
    const server = http.createServer();

    /* websocket server reference */
    const wss = new WebSocket.Server({ server });

    /* handle new connections */
    wss.on("connection", socket => {
        socket.on("message", data => {
            try {
                const message = JSON.parse(data.toString());
                const type = mapMessageType(message);

                if (handlers[type]) {
                    return handlers[type](socket, message);
                }
            } catch (e) {
                log.error(e);
            }
        });

        socket.on("close", () => {
            const store = LiveSearchStore.instance();
            const search = store.lookup(socket);
            if (search) {
                log.info(`Client disconnected for search: query="${search.query}"`);
                store.removeBySocket(socket);
            }
        });
    });

    const port = process.env.PORT || 9300;
    return new Promise((resolve, reject) => server.listen(port, () => resolve(port)));
};
