import { EntityStore } from "src/entity";
import * as log from "src/log";
import { Message, MessageHandler } from "src/message/types";
import * as WebSocket from "ws";

export const clearEntities: MessageHandler = (socket: WebSocket, message: Message) => {
    const store = EntityStore.instance();
    store.clear();
    log.info(`Cleared Entities: size=${store.size}`);
};
