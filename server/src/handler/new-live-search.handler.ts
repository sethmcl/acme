import { EntityStore } from "src/entity";
import { LiveSearch } from "src/live-search/live-search";
import { LiveSearchStore } from "src/live-search/live-search-store";
import * as log from "src/log";
import { MessageHandler, NewLiveSearchMessage } from "src/message";

export const newLiveSearch: MessageHandler = (socket, message) => {
    const searchStore = LiveSearchStore.instance();
    const entityStore = EntityStore.instance();

    const { query } = (message as NewLiveSearchMessage).data;
    log.info(`New search request: query="${query}"`);

    // Create and store a new live search object
    const search = new LiveSearch({ socket, query });
    searchStore.insert(search);

    // Get search results based on current entity data
    const initialResults = entityStore.search(search.terms);
    search.init(initialResults);
};
