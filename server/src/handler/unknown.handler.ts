import * as log from "src/log";
import { MessageHandler } from "src/message";

export const unknown: MessageHandler = (socket, message) => {
    log.warn(`Received unknown message type: ${message.type}; IGNORING`);
};
