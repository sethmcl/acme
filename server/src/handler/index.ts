import { MessageType } from "src/message";
import { clearEntities } from "./clear-entities.handler";
import { insertEntity } from "./insert-entity.handler";
import { newLiveSearch } from "./new-live-search.handler";
import { pauseLiveSearch } from "./pause-live-search.handler";
import { resumeLiveSearch } from "./resume-live-search.handler";
import { unknown } from "./unknown.handler";

export const handlers = {
    [MessageType.ClearEntities]: clearEntities,
    [MessageType.InsertEntity]: insertEntity,
    [MessageType.NewLiveSearch]: newLiveSearch,
    [MessageType.PauseLiveSearch]: pauseLiveSearch,
    [MessageType.ResumeLiveSearch]: resumeLiveSearch,
    [MessageType.Unknown]: unknown,
};
