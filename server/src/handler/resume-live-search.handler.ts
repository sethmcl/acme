import * as log from "src/log";
import { MessageHandler } from "src/message";

export const resumeLiveSearch: MessageHandler = (socket, message) => {
    log.warn(`Got message type: ${message.type}`);
};
