import { Entity, EntityStore } from "src/entity";
import { LiveSearchStore } from "src/live-search/live-search-store";
import * as log from "src/log";
import { Message, MessageHandler } from "src/message/types";
import * as WebSocket from "ws";

export const insertEntity: MessageHandler = (socket: WebSocket, message: Message) => {
    const entity = new Entity(message.data);
    const store = EntityStore.instance();
    const inserted = store.insert(entity);
    log.info(`Insert Entity [${entity.id}]: inserted=${inserted} size=${store.size}`);

    // Check if this entity matches any of the currently active live searches
    for (const search of LiveSearchStore.instance().all) {
        if (search.doesEntityMatch(entity)) {
            log.info(`Search for "${search.query}" has new result: ${entity.id}`);
            search.onNewResult(entity);
        }
    }
};
