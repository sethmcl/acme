export const write = msg => console.log(msg);
export const info = msg => write(`[INFO]  ${msg}`);
export const error = msg => write(`[ERROR] ${msg}`);
export const warn = msg => write(`[WARN]  ${msg}`);
