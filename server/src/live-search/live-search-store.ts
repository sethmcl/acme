import WebSocket from "ws";
import { LiveSearch } from "./live-search";

export class LiveSearchStore {
    public static instance() {
        if (!LiveSearchStore._instance) {
            LiveSearchStore._instance = new LiveSearchStore();
        }

        return LiveSearchStore._instance;
    }

    private static _instance: LiveSearchStore;

    public indexBySocket: Map<WebSocket, LiveSearch> = new Map();

    public insert(search: LiveSearch) {
        this.indexBySocket.set(search.socket, search);
    }

    public lookup(socket: WebSocket) {
        return this.indexBySocket.get(socket);
    }

    public removeBySocket(socket: WebSocket) {
        this.indexBySocket.delete(socket);
    }

    public get all() {
        return this.indexBySocket.values();
    }
}
