import { Entity } from "src/entity";
import { LiveSearch } from "./live-search";

describe("LiveSearch", () => {
    describe("Entity matching", () => {
        it("Should match entity", () => {
            const search = new LiveSearch({ query: "seth abc", socket: null });
            const entity = new Entity({
                matching_terms: ["seth", "file", "dropbox", "result", "abc"],
                source: "files",
            });

            expect(search.doesEntityMatch(entity)).toBe(true);
        });

        it("Should not match entity", () => {
            const search = new LiveSearch({ query: "seth abc", socket: null });
            const entity = new Entity({
                matching_terms: ["seth", "file", "dropbox", "result"],
                source: "files",
            });

            expect(search.doesEntityMatch(entity)).toBe(false);
        });
    });
});
