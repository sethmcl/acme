import * as WebSocket from "ws";
import { LiveSearch } from "./live-search";
import { LiveSearchStore } from "./live-search-store";

describe("LiveSearchStore", () => {
    let store: LiveSearchStore;

    const mockSocket = (): WebSocket => {
        /*tslint:disable:no-object-literal-type-assertion*/
        return {} as WebSocket;
    };

    it("Insert/lookup", () => {
        store = new LiveSearchStore();
        const socket = mockSocket();
        const search = new LiveSearch({ query: "abc", socket });
        store.insert(search);
        expect(store.lookup(socket)).toBe(search);
    });
});
