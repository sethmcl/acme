import { Entity } from "src/entity/entity";
import * as log from "src/log";
import { InitialSearchResultsMessage, NewLiveSearchResultMessage, sendFactory } from "src/message";
import uuidv4 from "uuid/v4";
import * as WebSocket from "ws";

export interface LiveSearchOptions {
    socket: WebSocket;
    query: string;
}

export class LiveSearch {
    private _id: string;
    private _socket: WebSocket;
    private _sendToSocket: (data: any) => void;
    private _query: string;
    private _results: Set<string>;

    constructor(options: LiveSearchOptions) {
        this._id = uuidv4();
        this._socket = options.socket;
        this._sendToSocket = sendFactory(options.socket);
        this._query = options.query;
        this._results = new Set();
    }

    public init(results: Entity[]) {
        const insertedResults = this.addResults(results);
        this._sendToSocket(
            new InitialSearchResultsMessage({
                liveSearchId: this._id,
                results: insertedResults,
            }),
        );
    }

    public onNewResult(result: Entity) {
        if (this.addResult(result)) {
            this._sendToSocket(
                new NewLiveSearchResultMessage({
                    liveSearchId: this._id,
                    result,
                }),
            );
        }
    }

    public addResult(result: Entity) {
        if (this._results.has(result.id)) {
            log.warn(`LiveSearch::addResult Result ${result.id} already exists in LiveSearch. Aborting add.`);
            return false;
        }

        this._results.add(result.id);
        return true;
    }

    public addResults(results: Entity[]) {
        return results.filter(r => this.addResult(r));
    }

    public get query() {
        return this._query;
    }

    public get id() {
        return this._id;
    }

    public get socket() {
        return this._socket;
    }

    public get terms() {
        return this.query.split(" ");
    }

    public doesEntityMatch(entity: Entity) {
        // Check if all search terms appear in entity.match_terms.
        // Basically, we are iterating through terms for this search, and
        // then filter out any term that matches the entity.matching_terms.
        // At the end, if the resulting array is empty, then know know all terms match,
        // and we have a hit!
        return this.terms.filter(term => entity.matching_terms.indexOf(term) === -1).length === 0;
    }
}
