#!/bin/bash

set -e
set -x

CWD=$(cd $(dirname $0) && pwd -P)
yarn jest --config $CWD/jest-e2e.json --runInBand --verbose --detectOpenHandles $@