#!/bin/bash

set -e
set -x

CWD=$(cd $(dirname $0) && pwd -P)
cd $CWD/..
yarn jest --runInBand src/