import WebSocket from "ws";

export class Client {
    /** Connection Config */
    public port = process.env.PORT || 9300;
    public host = process.env.HOST || "localhost";

    private _socket: WebSocket;
    private _isConnected: boolean = false;
    private _incomingMessages: any[] = [];

    constructor(options: Partial<Client> = {}) {
        Object.assign(this, options);
    }

    public connect() {
        return new Promise(resolve => {
            this._socket = new WebSocket(`ws://${this.host}:${this.port}`);

            this._socket.on("open", () => {
                this._isConnected = true;
                resolve();
            });

            this._socket.on("close", () => {
                this._isConnected = false;
            });

            this._socket.on("error", err => console.error("Socket error:", err));

            this._socket.on("message", data => {
                this._incomingMessages.push(JSON.parse(data.toString()));
            });
        });
    }

    public disconnect() {
        if (!this.isConnected) {
            return Promise.resolve();
        }

        return new Promise((resolve, reject) => {
            this._socket.onclose = resolve;
            this._socket.close();
        });
    }

    public assertConnection = (fn: () => void) => {
        if (!this.isConnected) {
            throw new Error("Client is not connected, cannot continue operation");
        }

        return fn();
    };

    public sendMessage(type, data: Record<string, any>) {
        return this.assertConnection(() => this._socket.send(JSON.stringify({ data, type })));
    }

    public insertEntity(source: string, data: Record<string, any>) {
        this.sendMessage("insert-entity", { source, ...data });
    }

    public deleteAllEntities() {
        this.sendMessage("clear-entities", {});
    }

    public search(query: string) {
        this.sendMessage("new-live-search", { query });
    }

    public get nextMessage() {
        const message = this._incomingMessages[0];
        this._incomingMessages = this._incomingMessages.slice(1);
        return message;
    }

    public get hasMessage() {
        return Boolean(this._incomingMessages.length);
    }

    public get isConnected() {
        return this._isConnected;
    }

    public waitForNextMessage() {
        if (this.hasMessage) {
            return Promise.resolve(this.nextMessage);
        }

        return new Promise((resolve, reject) => {
            const startTime = Date.now();
            const timeout = 1000 * 3;

            const checkForMessages = () => {
                if (this.hasMessage) {
                    return resolve(this.nextMessage);
                }

                if (Date.now() - startTime < timeout) {
                    setTimeout(checkForMessages, 100);
                } else {
                    reject(new Error("Timeout waiting for message"));
                }
            };

            checkForMessages();
        });
    }
}
