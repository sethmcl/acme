import ms = require("ms");

export const waitFor = async (duration: string) =>
    new Promise(resolve => {
        setTimeout(resolve, ms(duration));
    });
