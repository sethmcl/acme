import { Client } from "test/lib/client";

describe("Simple search scenarios", () => {
    let client: Client;

    beforeEach(async () => {
        client = new Client();
        await client.connect();
    });

    afterEach(async () => {
        client.deleteAllEntities();
        await client.disconnect();
    });

    it("Can search to find known entities", async () => {
        client.insertEntity("dropbox", { file: "abc", author: "seth", matching_terms: ["seth", "abc"] });
        client.insertEntity("dropbox", { file: "xyz", author: "seth", matching_terms: ["seth", "xyz"] });
        client.insertEntity("dropbox", { file: "reports", author: "dave", matching_terms: ["dave", "reports"] });
        client.search("seth");

        const message = await client.waitForNextMessage();

        expect(message.type).toBe("initial-search-results");
        expect(message.data.results.length).toBe(2);

        expect(message.data.results[0].source).toBe("dropbox");
        expect(message.data.results[0].file).toBe("abc");
        expect(message.data.results[0].author).toBe("seth");

        expect(message.data.results[1].source).toBe("dropbox");
        expect(message.data.results[1].file).toBe("xyz");
        expect(message.data.results[1].author).toBe("seth");
    });

    it("Can receive live updates", async () => {
        client.search("seth");
        let message = await client.waitForNextMessage();
        expect(message.data.results.length).toBe(0);
        client.insertEntity("dropbox", { file: "expenses", author: "seth", matching_terms: ["seth", "expenses"] });
        message = await client.waitForNextMessage();
        expect(message.type).toBe("new-live-search-result");
        expect(message.data.result.source).toBe("dropbox");
        expect(message.data.result.file).toBe("expenses");
        expect(message.data.result.author).toBe("seth");
    });

    it("Can receive correct live updates after running multiple searches", async () => {
        let message;
        client.search("usa");
        message = await client.waitForNextMessage();
        expect(message.data.results.length).toBe(0);

        client.search("seth");
        message = await client.waitForNextMessage();
        expect(message.data.results.length).toBe(0);

        client.insertEntity("dropbox", { file: "expenses", author: "seth", matching_terms: ["seth", "expenses"] });
        client.insertEntity("dropbox", { file: "expenses", author: "usa", matching_terms: ["usa", "expenses"] });
        message = await client.waitForNextMessage();
        expect(message.type).toBe("new-live-search-result");
        expect(message.data.result.source).toBe("dropbox");
        expect(message.data.result.file).toBe("expenses");
        expect(message.data.result.author).toBe("seth");
    });
});
