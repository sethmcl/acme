# Acme Search Project

This README is broken down into several parts:

- [Instructions for running and testing the project locally](#run-the-project)
- [Project design and scoping decisions](#design-and-scope)
- [Technology and implementation choices](#implementation-choices)
- [Areas to improve, given extended development time](#future-work)
- [Final thoughts](#final-thoughts)

## Run the Project

**Requirements**

- A unix-based OS that includes the `make` tool (built and tested on MacOS)
- Node.js (built and tested with v10.13.0)
- Yarn (built and tested with v1.15.2)
- A modern browser (tested with stable Chrome, Safari and Firefox on MacOS)

**Steps to run code**

```bash
# Get the code
git clone git@bitbucket.org:sethmcl/acme.git ~/acme

# Start the server
make -C ~/acme/server

# Start the factory to produce mock data
make -C ~/acme/factory

# Start the client
make -C ~/acme/client
```

After everything is up and running, navigate to http://localhost:8080 to begin playing with the UI. A few tips:

- Search is implemented as simple boolean AND against the `matching_terms` field in the data schema. For example, if you search for "expense report", items will match if they contain both "expense" and "report" in their `matching_terms`.
- If you search for "dropbox", all dropbox items will be returned. You can replace "dropbox" with the other data source names as well ("calendar", "contacts", "slack", "twitter").
- If you search for "all-data", ALL of the data will match.

**Steps to run tests**

```bash
# run unit tests for the server
make test -C ~/acme/server

# run integration tests for the server
make -C ~/acme/server # must first manually start server
make e2e -C ~/acme/server # launches integration tests
```

_Note: integration tests will reset all in-memory data, so be sure to restart the factory after running tests if you want to have mock data available to play with_.

## Design and Scope

Based on the project requirements, I chose to focus on building a UI application which can display live search results across a variety of data sources. I started by creating some mocks of the different screens I thought I would need (see the [ux mocks](./ux-mocks/mocks.pdf) here for reference).

After having designed the basic ux, I decided to break the project down into three major pieces:

- [Client](./client)
- [Server](./server)
- [Data Factory](./factory)

(mock) Data is pushed to the server by the factory. The server stores all data in memory, and makes it available to clients by fulfilling search requests. Client searches are "live" in the sense that when new matching data is received by the server, the server will push this data to the client as a new "live result".

I began by building the data factory and server. After these pieces were working and tested, I created the web client UI.

## Implementation Choices

- TypeScript is used in all components (client, server, and data factory)
- The server and data factory are node.js applications
- The client is a single page application written with React
- Jest is used for running unit and integration tests

## Future Work

Naturally, there are a lot of areas that need more work if we wanted to turn this into a real product. Some areas that come to mind:

**Instrumentation/analytics**. I did not have time to add any instrumentation, but this would be critical to understand how users are engaging with the product.

**Logging**. Logging is implemented in a very simple way by printing to stdout/stderr. Probably the next step I would take is to log JSON objects which are easier to interact with programatically and can also be stored somewhere for searching and later analysis.

**Test coverage**. Most of the more interesting logic is actually on server side, so that's where I focused my test efforts. But in future, I would want to add more test coverage to server and also add some test coverage in client-side as well.

**Favorites/pinned items**. It would be nice to be able to "pin" items to create collections of interesting content. I put a little thought into this in my initial ux mocks, but ran out of time to implement the functionality.

**Pause/resume live updates**. I think live search updates is powerful, but it can be a little disorienting if you are trying to read content that keeps shifting due to new content coming in. I thought about this a bit, and one idea I had was to offer a button to pause/resume live updates. Additionally, I was thinking I could automatically pause the updates when the user scrolls, because this might be an indication the user is trying to read something.

**Concurrent searches**. Real power users may want to have multiple searches running concurrently. This is kind of possible now simply by opening multiple browser tabs, but we could probably do something more compelling.

**Deep linking**. I did not have time to implement any type of detail view, to show more information if a user wants to drill-down into a specific piece of content. It would be nice to link to files in dropbox, link to other tweets by same user in same time perioud, etc.

## Final Thoughts

When I began the project, I initially estimated it would take me about 20 hours. I was quite close -- I believe I spent 19 hours on the task. It was a fun exercise, and I really enjoyed the experience :)
