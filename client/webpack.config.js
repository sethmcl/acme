const WebpackNotifierPlugin = require("webpack-notifier");
const HtmlWebpackPlugin = require("html-webpack-plugin");
const CircularDependencyPlugin = require("circular-dependency-plugin");
const { resolve } = require("path");

module.exports = () => ({
    devServer: {
        hot: true,
        host: "0.0.0.0",
        port: 8080,
        contentBase: "./src",
    },

    devtool: "cheap-module-eval-source-map",

    entry: {
        app: "./src/main.ts",
    },

    module: {
        rules: [
            /* typescript files */
            {
                test: /\.tsx?$/,
                use: [{ loader: "ts-loader" }],
            },

            /* html files */
            {
                test: /\.html$/,
                loader: "html-loader",
            },

            /* image/font files */
            {
                test: /\.(eot|png|svg|ttf|woff|woff2)$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "public/assets/[name]-[hash:8].[ext]",
                        },
                    },
                ],
            },

            /* less files */
            {
                test: /\.less$/,
                exclude: /\.module\.less$/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            modules: false,
                            importLoaders: 1,
                            localIdentName: "[name]__[local]___[hash:base64:5]",
                            sourceMap: true,
                        },
                    },
                    {
                        loader: "less-loader",
                        options: {
                            loaderOptions: {
                                sourceMap: true,
                                javascriptEnabled: true,
                            },
                        },
                    },
                    {
                        loader: "alias-resolve-loader",
                        options: {
                            alias: {
                                "@app": "./src/app",
                            },
                        },
                    },
                ],
            },

            /* less module files */
            {
                test: /\.module\.less$/,
                use: [
                    "style-loader",
                    {
                        loader: "css-loader",
                        options: {
                            modules: true,
                            importLoaders: 1,
                            localIdentName: "[name]__[local]___[hash:base64:5]",
                            sourceMap: true,
                        },
                    },
                    {
                        loader: "less-loader",
                        options: {
                            loaderOptions: {
                                sourceMap: true,
                                javascriptEnabled: true,
                            },
                        },
                    },
                    {
                        loader: "alias-resolve-loader",
                        options: {
                            alias: {
                                "@app": "./src/app",
                            },
                        },
                    },
                ],
            },
        ],
    },

    output: {
        filename: "[name].js",
        chunkFilename: "[name].chunk.js",
        publicPath: "/",
    },

    plugins: [
        new WebpackNotifierPlugin({
            title: require("./package.json").name,
            excludeWarnings: true,
        }),

        new HtmlWebpackPlugin({
            template: "src/index.ejs",
            filename: "index.html",
        }),

        new CircularDependencyPlugin({
            exclude: /node_modules/,
            failOnError: true,
        }),
    ],

    resolve: {
        extensions: [".ts", ".tsx", ".js"],
        alias: {
            src: resolve("./src"),
        },
    },

    target: "web",
});
