import * as React from "react";
import { render } from "react-dom";
import "src/app/favicons";
import { Application } from "./app/application";
import { SearchService } from "./service";

const service = new SearchService();
render(React.createElement(Application, { service }), document.querySelector("#app"));
