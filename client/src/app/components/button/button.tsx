import * as React from "react";
import style from "./button.module.less";

interface Props {
    onClick?: (event: React.MouseEvent<HTMLButtonElement>) => void;
    type?: "submit" | "reset" | "button";
}

export const Button: React.SFC<Props> = props => (
    <button type={props.type} className={style.component} onClick={props.onClick}>
        {props.children}
    </button>
);

Button.defaultProps = {
    onClick: () => {},
    type: "button",
};
