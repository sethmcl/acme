import * as React from "react";
import { Entity } from "src/entity";
import { formatNumberWithCommas } from "src/util/strings";
import { EntityCard } from "../entity-card";
import { Icon } from "../icon";
import style from "./result-set.module.less";

interface Props {
    /** Result entities */
    results: Entity[];

    /** Title */
    title: string;

    /** Icon, for some added flair. Flair is not required, but it IS highly encouraged. */
    icon?: string;
}

export const ResultSet: React.SFC<Props> = props => (
    <ul className={style.component}>
        <li className={style.header}>
            {props.icon && <Icon name={props.icon} className={style.icon} />}
            <span className={style.title}>{props.title}</span>
            <span className={style.count}>({formatNumberWithCommas(props.results.length)})</span>
        </li>
        <li>
            <ul>
                {props.results.map(result => (
                    <li key={result.id}>
                        <EntityCard entity={result} />
                    </li>
                ))}
            </ul>
        </li>
    </ul>
);
