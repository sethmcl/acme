import cx from "classnames";
import * as React from "react";
import { Button } from "../button";
import style from "./search-bar.module.less";

interface Props {
    /** Append this CSS classname */
    className?: string;

    /** Callback fired when user presses enter (form submit) */
    onSearch?: (query: string) => void;

    /** Callback fired when value of input box changes */
    onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;

    /** Pin to top of screen */
    pinned?: boolean;

    /** Show search button */
    showButton?: boolean;

    /** Automatically focus */
    autoFocus?: boolean;

    /** Default value */
    value?: string;
}

export const SearchBar: React.SFC<Props> = props => {
    const [value, setValue] = React.useState(null);

    React.useEffect(() => {
        if (props.value && !value) {
            setValue(props.value);
        }
    }, [props.value]);

    const onInputChange = event => {
        setValue(event.target.value);
        props.onChange(event);
    };

    const onFormSubmit = event => {
        event.preventDefault();

        if (event.currentTarget[0].value && event.currentTarget[0].value.length) {
            props.onSearch(value);
        }
    };

    return (
        <form
            className={cx(props.className, style.component, { [style.pinned]: props.pinned })}
            onSubmit={onFormSubmit}
        >
            <input
                type="text"
                placeholder="Enter a person or company, then press enter to search..."
                onChange={onInputChange}
                value={value || ""}
                autoFocus={props.autoFocus}
            />
            {props.showButton && <Button type="submit">Search</Button>}
        </form>
    );
};

SearchBar.defaultProps = {
    onSearch: () => {},
    onChange: () => {},
    pinned: false,
    showButton: true,
    autoFocus: true,
};
