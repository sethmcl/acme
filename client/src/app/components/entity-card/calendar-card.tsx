import moment from "moment";
import * as React from "react";
import { Entity } from "src/entity";
import { BaseCard } from "./base-card";

interface Props {
    entity: Entity;
}

export const CalendarCard: React.SFC<Props> = ({ entity }) => {
    const title = entity.title;
    const location = entity.location ? entity.location : "unknown location";
    const date = entity.date ? moment(entity.date).format("YYYY-MM-DD") : "";
    const time = entity.date ? moment(entity.date).format("h:mm a") : "";

    const body = (
        <table className="small-text">
            <tbody>
                <tr>
                    <th>attendees</th>
                </tr>
                <tr>
                    <td>{entity.invitees}</td>
                </tr>
            </tbody>
        </table>
    );
    return (
        <BaseCard
            leftBigLabel={title}
            leftSmallLabel={location}
            rightBigLabel={date}
            rightSmallLabel={time}
            body={body}
        />
    );
};
