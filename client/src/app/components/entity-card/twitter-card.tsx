import moment from "moment";
import * as React from "react";
import { Entity } from "src/entity";
import { BaseCard } from "./base-card";

interface Props {
    entity: Entity;
}

export const TwitterCard: React.SFC<Props> = ({ entity }) => {
    const user = entity.user ? entity.user : "(unknown)";
    const date = entity.timestamp ? moment(entity.timestamp).format("YYYY-MM-DD") : "";
    const time = entity.timestamp ? moment(entity.timestamp).format("h:mm a") : "";
    const body = `"${entity.message}"`;

    return <BaseCard leftBigLabel={user} rightBigLabel={date} rightSmallLabel={time} body={<span>{body}</span>} />;
};
