import cx from "classnames";
import * as React from "react";
import { Icon } from "../icon";
import style from "./entity-card.module.less";

interface Props {
    leftBigLabel: string;
    leftSmallLabel: string;
    rightBigLabel: string;
    rightSmallLabel: string;
    body: JSX.Element;
    icon: string;
}

export const BaseCard: React.SFC<Partial<Props>> = props => {
    const [loading, setLoading] = React.useState(true);

    React.useEffect(() => {
        setTimeout(() => setLoading(false), 100);
    });

    return (
        <div className={cx(style.component, { [style.loading]: loading })}>
            <div className={cx(style.row, style.big)}>
                {props.icon && <Icon name={props.icon} className={style.icon} />}
                <span className={style.left}>{props.leftBigLabel}</span>
                <span className={style.right}>{props.rightBigLabel}</span>
            </div>
            <div className={cx(style.row, style.small)}>
                <span className={style.left}>{props.leftSmallLabel}</span>
                <span className={style.right}>{props.rightSmallLabel}</span>
            </div>
            {props.body && <div className={style.body}>{props.body}</div>}
        </div>
    );
};
