import moment from "moment";
import * as React from "react";
import { Entity } from "src/entity";
import { BaseCard } from "./base-card";

interface Props {
    entity: Entity;
}

export const SlackCard: React.SFC<Props> = ({ entity }) => {
    const author = entity.author ? `@${entity.author}` : "(unknown)";
    const channel = entity.channel ? `sent to ${entity.channel}` : "";
    const date = entity.timestamp ? moment(entity.timestamp).format("YYYY-MM-DD") : "";
    const time = entity.timestamp ? moment(entity.timestamp).format("h:mm a") : "";
    const body = entity.message ? `"${entity.message}"` : "";

    return (
        <BaseCard
            leftBigLabel={author}
            leftSmallLabel={channel}
            rightBigLabel={date}
            rightSmallLabel={time}
            body={<span>{body}</span>}
        />
    );
};
