import moment from "moment";
import * as React from "react";
import { Entity } from "src/entity";
import { BaseCard } from "./base-card";

interface Props {
    entity: Entity;
}

export const DropboxCard: React.SFC<Props> = props => {
    const { title, created, shared_with } = props.entity;
    let sharedLabel = "not shared";
    if (shared_with && shared_with.length) {
        sharedLabel = `shared with ${shared_with.join(", ")}`;
    }

    return (
        <BaseCard
            leftBigLabel={title}
            rightBigLabel={moment(created).format("YYYY-MM-DD")}
            leftSmallLabel={sharedLabel}
        />
    );
};
