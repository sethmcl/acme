import * as React from "react";
import { Entity, EntitySource } from "src/entity";
import { CalendarCard } from "./calendar-card";
import { ContactsCard } from "./contacts-card";
import { DropboxCard } from "./dropbox-card";
import { SlackCard } from "./slack-card";
import { TwitterCard } from "./twitter-card";
import { UnknownCard } from "./unknown-card";

interface Props {
    entity: Entity;
}

export const EntityCard: React.SFC<Props> = props => {
    const cards = {
        [EntitySource.Calendar]: CalendarCard,
        [EntitySource.Contacts]: ContactsCard,
        [EntitySource.Dropbox]: DropboxCard,
        [EntitySource.Slack]: SlackCard,
        [EntitySource.Twitter]: TwitterCard,
    };

    let CardComponent = cards[props.entity.source];

    if (!CardComponent) {
        // No matching source, so use unknown card type
        CardComponent = UnknownCard;
    }

    return React.createElement(CardComponent, { entity: props.entity });
};
