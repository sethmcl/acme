import moment from "moment";
import * as React from "react";
import { Entity } from "src/entity";
import { BaseCard } from "./base-card";

interface Props {
    entity: Entity;
}

export const ContactsCard: React.SFC<Props> = ({ entity }) => {
    const name = entity.name ? entity.name : "(unknown)";
    const company = entity.company;
    const date = entity.last_contact ? moment(entity.last_contact).format("YYYY-MM-DD") : "";
    const emails = (entity.emails || []).join("; ");
    const phones = (entity.phones || []).join("; ");
    const body = (
        <table className="small-text">
            <tbody>
                <tr>
                    <th>email</th>
                    <td>{emails}</td>
                </tr>
                <tr>
                    <th>phone</th>
                    <td>{phones}</td>
                </tr>
            </tbody>
        </table>
    );

    return <BaseCard leftBigLabel={name} leftSmallLabel={company} rightBigLabel={date} body={body} />;
};
