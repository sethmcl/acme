import * as React from "react";
import { Entity } from "src/entity";

interface Props {
    entity: Entity;
}

export const UnknownCard: React.SFC<Props> = props => <div>Unknown Item</div>;
