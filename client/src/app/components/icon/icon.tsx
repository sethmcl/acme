import cx from "classnames";
import * as React from "react";
import style from "./icon.module.less";

interface Props {
    /** Name of icon to show */
    name: string;

    /** Extra classname to add */
    className: string;
}

export const Icon: React.SFC<Props> = props => (
    <div className={cx(style[props.name], style.component, props.className)} />
);
