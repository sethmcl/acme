import memoize from "memoize-one";
import * as React from "react";
import { ResultSet, SearchBar } from "src/app/components";
import { Entity } from "src/entity";
import style from "./search.module.less";

interface Props {
    /** Callback when new search query is executed */
    onSearch: (query: string) => void;

    /** Result data */
    results: Entity[];

    /** Current search query */
    query: string;
}

const getSourceData = memoize((source: string, results: Entity[]) =>
    results.filter(entity => entity.source === source),
);

export const SearchPage = (props: Props) => {
    const calendar = getSourceData("calendar", props.results);
    const contacts = getSourceData("contacts", props.results);
    const dropbox = getSourceData("dropbox", props.results);
    const slack = getSourceData("slack", props.results);
    const twitter = getSourceData("twitter", props.results);

    return (
        <div className={style.component}>
            <SearchBar onSearch={props.onSearch} pinned={true} value={props.query} showButton={false} />
            <div className={style.resultContainer}>
                <ResultSet results={dropbox} title="Dropbox" icon="dropbox" />
                <ResultSet results={slack} title="Slack" icon="slack" />
                <ResultSet results={twitter} title="Twitter" icon="twitter" />
                <ResultSet results={calendar} title="Calendar" icon="calendar" />
                <ResultSet results={contacts} title="Contacts" icon="user" />
            </div>
        </div>
    );
};
