import * as React from "react";
import { SearchBar } from "src/app/components";
import style from "./home.module.less";

interface Props {
    onSearch: (query: string) => void;
}

export const HomePage = (props: Props) => (
    <div className={style.component}>
        <div className={style.logo}>Acme Search</div>
        <SearchBar onSearch={props.onSearch} className={style.searchBar} />
    </div>
);
