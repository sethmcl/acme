import * as React from "react";
import { HomePage, SearchPage } from "src/app/pages";
import { SearchService } from "src/service";
import "./application.less";

interface Props {
    service: SearchService;
}

export const Application: React.SFC<Props> = ({ service }) => {
    const [state, setState] = React.useState({ showHome: true, query: "", results: [] });

    const onSearch = query => {
        console.log("on search", query);
        setState({
            showHome: false,
            query,
            results: [],
        });

        history.replaceState(history.state, document.title, `?${encodeURIComponent(query)}`);
        service.search(query);
    };

    React.useEffect(() => {
        service.onData = results => setState(s => ({ ...s, results }));
        if (window.location.search.length > 1) {
            const query = decodeURIComponent(window.location.search.slice(1));
            if (query !== state.query) {
                onSearch(query);
            }
        }
    });

    return (
        <div>
            {state.showHome && <HomePage onSearch={onSearch} />}
            {!state.showHome && <SearchPage onSearch={onSearch} results={state.results} query={state.query} />}
        </div>
    );
};
