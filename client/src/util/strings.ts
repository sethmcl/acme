export const formatNumberWithCommas = (n: number | string) => {
    let input: string;

    if (typeof n === "string") {
        input = n;
    }

    if (typeof n === "number") {
        input = n.toString();
    }

    if (typeof input === "string" && input.length) {
        return input.replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    return n;
};
