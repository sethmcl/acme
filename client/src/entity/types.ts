export enum EntitySource {
    Dropbox = "dropbox",
    Calendar = "calendar",
    Contacts = "contacts",
    Slack = "slack",
    Twitter = "twitter",
}
