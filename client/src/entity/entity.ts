export class Entity {
    /* Unique identifier */
    public id: string = "";

    /* Data source (dropbox, calendar, etc) */
    public source: string = "";

    /* Used for simple search matching */
    public matching_terms: string[] = [];

    /* Flexible schema */
    [prop: string]: any;

    constructor(data: Partial<Entity> = {}) {
        Object.assign(this, { ...data });
    }
}
