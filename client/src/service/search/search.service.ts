import { Entity } from "src/entity";
import { Message, MessageType } from "../../message";
import { WsClient } from "./ws-client";

export class SearchService {
    private _client: WsClient;
    private _results: Entity[] = [];

    constructor() {
        console.log("Search service connection");
        this._client = new WsClient({
            onConnect: this.onConnect,
            onDisconnect: this.onDisconnect,
            onMessage: this.onMessage,
        });

        this._client.connect();
    }

    public onData: (results: Entity[]) => void = () => {};

    public onConnect = () => {
        console.log("connected");
    };

    public onDisconnect = () => {
        alert(
            "You have been disconnected form the Search Service. Apologies for this unattractive error message. Please reload your browser to try again!",
        );
    };

    public onMessage = (message: Message) => {
        switch (message.type) {
            case MessageType.InitialSearchResults:
                this._results = message.data.results;
                this.onData(this._results);
                break;

            case MessageType.NewLiveSearchResult:
                this._results = [message.data.result, ...this._results];
                this.onData(this._results);
                break;
        }
    };

    public search(query: string) {
        this._client.sendMessage({
            type: MessageType.NewLiveSearch,
            data: { query },
        });
        this._results = [];
    }
}
