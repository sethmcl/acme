import { Message } from "src/message";

export class WsClient {
    private _socket: WebSocket = null;
    private _sendQueue = [];

    constructor(options: Partial<WsClient> = {}) {
        Object.assign(this, options);
    }

    public onConnect: () => void = () => {};
    public onDisconnect: () => void = () => {};
    public onMessage: (message: Message) => void = () => {};

    public connect() {
        // TODO: server origin should not be hardcoded
        this._socket = new WebSocket("ws://localhost:9300");

        this._socket.onopen = event => {
            this.flushSendQueue();
            this.onConnect();
        };

        this._socket.onclose = event => {
            this.onDisconnect();
        };

        this._socket.onmessage = event => {
            let message;

            try {
                message = JSON.parse(event.data);
            } catch (e) {
                console.error("Unable to parse message data as JSON");
                console.error(e);
            }

            this.onMessage(message as Message);
        };
    }

    public sendMessage = (message: any) => {
        if (this._socket.readyState === WebSocket.OPEN) {
            this.sendJson(message);
        } else {
            this._sendQueue.push(message);
        }
    };

    public sendJson = (message: any) => {
        this._socket.send(JSON.stringify(message));
    };

    public flushSendQueue() {
        for (const message of this._sendQueue) {
            this.sendJson(message);
        }

        this._sendQueue.length = 0;
    }
}
