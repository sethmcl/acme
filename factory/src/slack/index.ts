import * as faker from "faker";
import { Entity } from "src/entity";
import * as utils from "../utils";

const data = require("./static.json");
const attributeSource = utils.attributeSourceFn("slack");

export const getStaticData = (): Entity[] => {
    return data.map(attributeSource).map(utils.cleanProps);
};

export const spawn = () => {
    const channel = faker.commerce.department();
    const author = faker.name.firstName();
    const message = faker.lorem.sentences();
    const timestamp = new Date();
    const matching_terms = ["slack", "all-data", channel, author, ...message.split(" ")];

    return attributeSource(
        new Entity({
            author,
            channel,
            matching_terms,
            message,
            timestamp,
        }),
    );
};
