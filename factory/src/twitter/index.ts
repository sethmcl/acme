import * as faker from "faker";
import { Entity } from "src/entity";
import * as utils from "../utils";

const data = require("./static.json");
const attributeSource = utils.attributeSourceFn("twitter");

export const getStaticData = (): Entity[] => {
    return data.map(attributeSource).map(utils.cleanProps);
};

export const spawn = () => {
    const user = `@${faker.name.lastName()}`;
    const message = faker.lorem.sentence();
    const timestamp = new Date();
    const matching_terms = ["twitter", "all-data", user, ...message.split(" ")];

    return attributeSource(
        new Entity({
            matching_terms,
            message,
            timestamp,
            user,
        }),
    );
};
