import * as faker from "faker";
import { Entity } from "./entity";

export class Spawner {
    private send: (Entity) => void;
    private running: boolean;
    private factories: Array<() => Entity>;

    constructor(send: (Entity) => void, factories: Array<() => Entity>) {
        this.send = send;
        this.running = false;
        this.factories = factories;
    }

    public start() {
        this.running = true;
        this.spawn();
    }

    public stop() {
        this.running = false;
    }

    public spawn = () => {
        if (!this.running) {
            return;
        }

        const entity = faker.random.arrayElement(this.factories)();
        console.log(`spawned entity from "${entity.source}" [${entity.matching_terms.join(", ")}]`);
        this.send(entity);
        setTimeout(this.spawn, faker.random.number({ min: 2000, max: 10000 }));
    };
}
