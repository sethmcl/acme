import * as faker from "faker";
import { Entity } from "src/entity";
import * as utils from "../utils";

const data = require("./static.json");
const attributeSource = utils.attributeSourceFn("dropbox");

export const getStaticData = (): Entity[] => {
    return data.map(attributeSource).map(utils.cleanProps);
};

export const spawn = () => {
    const created = new Date();
    const matching_terms = ["dropbox", "all-data", ...utils.randomArrayOf(faker.company.bsNoun, 3, 10)];
    const path = utils.randomFilePath();
    const title = faker.company.bs();
    const shared_with = faker.random.arrayElement([[faker.fake("{{internet.email}}")], []]);

    return attributeSource(
        new Entity({
            created,
            matching_terms,
            path,
            shared_with,
            title,
        }),
    );
};
