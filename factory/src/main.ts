import WebSocket from "ws";
import { Spawner } from "./spawner";

/** Data Sources */
import * as calendar from "./calendar";
import * as contacts from "./contacts";
import * as dropbox from "./dropbox";
import * as slack from "./slack";
import * as twitter from "./twitter";

/** Connection Config */
const port = process.env.PORT || 9300;
const socket = new WebSocket(`ws://localhost:${port}`);

socket.on("open", () => {
    /** On initial connection, send all static data */
    const staticData = [
        calendar.getStaticData,
        contacts.getStaticData,
        dropbox.getStaticData,
        slack.getStaticData,
        twitter.getStaticData,
    ];

    for (const gsd of staticData) {
        gsd().forEach(sendEntity);
    }

    /** Start spawning random entities for demo purposes */
    const factories = [
        calendar.spawn,
        contacts.spawn,
        dropbox.spawn,
        dropbox.spawn,
        slack.spawn,
        slack.spawn,
        slack.spawn,
        slack.spawn,
        twitter.spawn,
        twitter.spawn,
        twitter.spawn,
        twitter.spawn,
        twitter.spawn,
    ];
    const spawner = new Spawner(sendEntity, factories);
    spawner.start();
});

socket.on("message", data => {
    console.log(`(from server) ${data}`);
});

socket.on("close", (code, reason) => {
    console.log(`Socket closed`);
    process.exit(0);
});

const sendMessage = (wSocket: WebSocket, type: string) => (data: Record<string, any>) => {
    wSocket.send(
        JSON.stringify({
            type,
            data,
        }),
    );
};

const sendEntity = sendMessage(socket, "insert-entity");
