import * as faker from "faker";
import { Entity } from "src/entity";
import * as utils from "../utils";

const data = require("./static.json");
const attributeSource = utils.attributeSourceFn("contacts");

export const getStaticData = (): Entity[] => {
    return data.map(attributeSource).map(utils.cleanProps);
};

export const spawn = () => {
    const company = faker.random.arrayElement(["Microsoft", "Google", "LinkedIn", "Pinterest", "Acme Inc"]);
    const emails = utils.randomArrayOf(() => faker.fake("{{internet.email}}"), 1, 2);
    const phones = utils.randomArrayOf(faker.phone.phoneNumber, 1, 1);
    const name = faker.fake("{{name.firstName}} {{name.lastName}}");
    const matching_terms = ["contacts", "all-data", ...name.split(" "), company];
    const last_contact = new Date();

    return attributeSource(
        new Entity({
            company,
            emails,
            last_contact,
            matching_terms,
            name,
            phones,
        }),
    );
};
