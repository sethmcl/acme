import * as faker from "faker";
import { Entity } from "src/entity";
import * as utils from "../utils";

const data = require("./static.json");
const attributeSource = utils.attributeSourceFn("calendar");

export const getStaticData = (): Entity[] => {
    return data.map(attributeSource).map(utils.cleanProps);
};

export const spawn = () => {
    const title = faker.fake("{{company.bsAdjective}} {{company.bsBuzz}} {{company.bsNoun}}");
    const names = utils.randomArrayOf(faker.fake.bind(faker, "{{name.firstName}} {{name.lastName}}"), 2, 8);
    const invitees = names.join(", ");
    const matching_terms = ["calendar", "all-data", ...names];
    const date = new Date();

    return attributeSource(
        new Entity({
            date,
            invitees,
            matching_terms,
            title,
        }),
    );
};
