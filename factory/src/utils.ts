import { Entity } from "./entity";
import * as faker from "faker";

export const attributeSourceFn = (source: string) => (entity: Entity) => new Entity({ ...entity, source });

export const cleanProps = (entity: Entity) => {
    const clone = new Entity(entity);
    delete clone.id;
    return clone;
};

export const randomArrayOf = <E = string>(factory: () => E, min = 1, max = 1): E[] => {
    const size = faker.random.number({ min, max });
    const values = [...new Array(size)];
    return values.map(factory);
};

export const randomFilePath = () => {
    const parts = ["", ...randomArrayOf(faker.company.bsNoun, 1, 4), faker.system.fileName()];
    return parts.join("/");
};
